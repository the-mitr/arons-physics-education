% !TEX root = arons-teaching-intro-physics.tex
\part{A Guide to Teaching \\for Learning and \\Understanding}

\chapter{Underpinnings}

\hlblue{Although they have been told, perhaps many times, that the integral can be interpreted as an area, the idea has not registered because it has not been made part of the individual student's concrete experience, and they have never had the opportunity to articulate the idea in their own words. p. 3} 

\hlblue{This [failure to have mastered reasoning involving ratios] disability, among the very large number of students who suffer from it, is one of the most serious impediments to their study of science. p. 4}


\hlblue{As a consequence, when they [students] find themselves outside the memorized situations, they are unable to solve problems that involve successive steps of arithmetical reasoning. p. 5}

\hlblue{Words connote as well as denote. P. 6}

\hlblue{A word of warning: If a teacher accepts casual use of the word per - particularly the incorrect and meaningless mass per volume, which was quoted in the preceding section he or she falls into a trap. Even though it contains only three letters, per is a technical term, and very few of those students who are having trouble with arithmetical reasoning know what it means. They inject it into a response only because they have a vague memory that per frequently turns up for some obscure reason in division, but they do not explicitly translate it into simpler words such as in, for each, corresponds to, ``goes with,'' combines with, is associated with.}

\hlblue{Again, a correct result is not firm evidence of understanding the line of reasoning. p. 8}

\hlblue{\dots linguistic elements play an essential, underlying role in the development of the capacity for arithmetical reasoning with ratios and proportion. \dots Lawson, Lawson, and Lawson (1984) who remark that a necessary \dots condition for the acquisition of proportional reasoning during adolescence is the prior internalization of key linguistic elements or argumentation. Failure to provide this linguistic experience in the schools underlies much of the difficulty students experience, and much of the fear of mathematics that we observe, at high school and college levels. p. 9}

\hlblue{A powerful way of helping students master a mode of reasoning is to allow them to view the same reasoning from more than one perspective. p. 9}


\hlblue{The graphical representation helps reinforce the insight that a given change along the horizontal axis produces a corresponding, fixed change along the vertical axis regardless of whether the shift is started at the origin or elsewhere along the line. p. 10}

\hlblue{\dots the necessity of taking roots instead of raising to powers turns out to be a formidable obstacle. p. 13}



\hlblue{``What significance, if any, do you see in the fact that the directions of the North Star and the shortest shadow of the vertical stick coincide? Is this simply an accident or might it have deeper meaning?'' p. 16}

\hlblue{The terms are so familiar and frequently invoked that the student has lost all sense of the fact that he or she does not really know what they mean. p. 17}


Clement, Lochhead, and Monk (1981); Rosnick and Clement (1980)] 
 
\hlblue{\dots the reversal is observed in problems that call for translation from pictures to equations or from data tables to equations. p. 17}


\hlblue{There are six times as many students as professors at this university. 
\begin{equation*}%
6S = P
\end{equation*}}

\hlblue{The most effective procedure is to give exercises in which the interpretations are traversed in both directions (words to symbols and symbols to words), and such questions should then be included on tests. The exercises should be given whenever the opportunity arises in subject matter being covered in the course, not as artificial episodes divorced from the course content} 


\hlblue{They tend to think that words are defined by synonyms found in a dictionary and, when it comes to concepts such as velocity and acceleration or force and mass, are completely unaware of the necessity of describing the actions and operations one executes, at least in principle, to give these terms scientific meaning. Since the words, to begin with, are metaphors, drawn from everyday speech, to which we give profoundly altered scientific meaning, only vaguely connected to the meaning in everyday speech, the students remain unaware of the alteration unless it is pointed to explicitly many times-not just once. p. 18}

\hlblue{The failure of many students to be aware when they do not fully comprehend the meaning of words and phrases in the context in which they occur underlies substantial portions of the ``illiteracy'' that we find currently deplored in many disciplines, not science alone. p. 19}



\hlblue{The problem should indeed be taken care of in the schools, but it has not been: and will not be taken care of in the near future, because the teachers, except for a very small minority, have not developed the necessary knowledge and skills. It must be strongly emphasized that this is not the fault of the teachers. The plight of the future teachers was blindly ignored when they were in college, and they were not helped to develop the abstract thinking and reasoning skills they need in their own classrooms. The vast majority of working teachers are individuals of dedication and good will, but they will not develop the necessary reasoning skills spontaneously. They need help, and this help must be forthcoming from the college-university level in both preservice and in-service training.  p. 20}

%\setlist[enumerate, 1]{before=\sffamily\color{OrangeRed}}
%\begin{enumerate}[label={\sffamily\color{OrangeRed}\arabic*.}]

\begin{enumerate}[label=\hlred{\arabic*.}]
\item \hlred{Suppose we make a saline solution by dissolving 176 g of salt in 5.00 L of water. (The resulting total volume of the solution is very nearly 5.00 L.)}
\begin{enumerate}[label={\hlred{(\alph*)}}]
\item \hlred{Calculate the concentration of the solution, explaining your reasoning briefly.}

The concentration of a solution is \emph{a measure of the amount of solute that has been dissolved in a given amount of solvent or solution.}\marginnote{\footnotesize \hlblue{Definition from} \href{https://chem.libretexts.org/Bookshelves/Introductory\_Chemistry/Book\%3A\_Chemistry\_for\_Allied\_Health\_(Soult)/08\%3A\_Properties\_of\_Solutions/8.01\%3A\_Concentrations\_of\_Solutions}{Chemistry Libretext}} 

The percent concentration for a given solution is described in terms of either the mass (when it is solid) or the volume of the solute (when it is liquid). The reason for this is that the concentration is in the form of a ratio, which is a \textbf{dimensionless number}. For this we require units of both the numerator and the denominator are the same. In our case, we have \SI{176}{\gram} of salt dissolved in \SI{5}{\litre} of water. Density of water is $\rho_{w} = \SI{1}{\gram\per\milli\litre}$, hence  \SI{5}{\litre} of water will weigh  \SI{5000}{\gram}. Now, let us find the concentration of the saline solution in terms of the mass:
\begin{align*}%
\text{Percent by mass} & = \frac{\text{mass of solute}}{\text{mass of solution}} \times 100\% && \text{substituting the values} \\
\text{Percent by mass} & = \frac{\text{176}}{\text{5000}} \times 100\% && \text{simplifying} \\
\text{Percent by mass} & = 3.52 \approx 3.5 \% && \text{simplifying}
\end{align*}
This means there is $\approx$ 3.5 grams of salt for every 100 grams of water. Or put in another way, there is  3.5 grams of salt \emph{per} 100 grams of water. In this calculation, as given in the problem, we have assumed both the volumes remain same, so volumetric concentration cannot be computed.

\item \hlred{Using the result obtained in part (a), calculate how many cubic centimeters of solution must be taken in order to supply 10.0 g of salt. Explain your reasoning briefly.}

We have the result that there is  3.5 grams of salt \emph{per} 100 grams of water. We have to find the water that will hold 10 grams of salt. Also, we know 1 gram of water is equal to \SI{1}{\centi\meter\cubed} of water. We use simple cross multiplication for the result:
\begin{align*}%
\frac{\%_{1}}{V_{1}}& = \frac{\%_{2}}{V_{2}} && \text{substituting} \\
\frac{3.5}{100}& = \frac{10.0}{V_{2}}&& \text{simplifying}\\
\therefore V_{2} &= \frac{100}{3.5} \times 10.0&& \text{simplifying}\\
V_{2} &= \SI{285.7}{\centi\meter\cubed}
\end{align*}
Thus, we would require a solution of \SI{285.7}{\centi\meter\cubed} to supply salt of 10.0 grams.

\item \hlred{Make up a problem that involves the density concept and in which the steps of reasoning are exactly parallel to the steps in (a) and (b) above. Be sure to select reasonable numerical values for the physical situation you describe. Present the solution of the problem, explaining the steps briefly.}
\end{enumerate}
\begin{tcolorbox}[title=Problem]
To form a sugar solution, \SI{100}{\centi\meter\cubed} of powdered sugar is dissolved in 2 litres of water. The density of sugar is \SI{1.59}{\gram\per\centi\meter\cubed}. (a) Find the concentration of the resulting solution. (b) What would be volume of solution that holds 55 grams of sugar?
\end{tcolorbox}
 

\item \hlred{We have a cylindrical container $A$ as illustrated in the \figr{fig-01-01}. A second container $B$ has the same shape as $A$, but the length scale, in all three dimensions, is larger by a factor of 1.80.}

\hlred{Answer the following questions by using appropriate scaling ratios only. There should be no appeal to formulas for areas or volumes of special shapes. Evaluate final results in decimal form. Explain your reasoning briefly in each instance.}
\begin{marginfigure}
\includegraphics[width=0.5\textwidth]{figures/ch-01-01.jpg}
\caption{A cylindrical container}
\label{fig-01-01}
\end{marginfigure}

\begin{enumerate}[label=\hlred{(\alph*)},leftmargin=1cm]
\item \hlred{How will the circumference $C$ of container $B$ compare with that of container $A$, that is, what is the numerical value of the ratio $C_{B}/C_{A}$?}

The circumference is a linear measure. Hence the ratio of $C_{B}/C_{A}$ will also scale linearly, that is $C_{B}/C_{A} = 1.80$.

\item \hlred{How many times larger is the cross-sectional area (i.e., the area of the base of $B$, denoted by $S_{B}$) than the cross-sectional area $S_{A}$?}

The cross-sectional area varies as square of the radius(length). Hence the ratio $S_{B}/S_{A} = 1.80^{2} = 3.24$.

\item \hlred{If $A$ contains \SI{25.0}{\litre} of water when filled to the brim, how many liters of water will $B$ hold when similarly filled?}

The volume area varies as cube of the radius(length). Hence the ratio $V_{B}/V_{A} = 1.80^{3} = 5.83$.

\end{enumerate}

\item \hlred{A replica is made of the statue of a man on horseback. The total volume of the replica is 0.51 the volume of the original.}

The ratio of the volume of replica $V_{r}$ and the original $V_{o}$ is 0.51. Hence $V_{o}/ V_{r} = 0.51$. Now the volume varies as cube of length, hence the length of the replica and original will vary as cube root of $\sqrt[3]{0.51} \approx 0.80$

\begin{enumerate}[label=\hlred{(\alph*)},leftmargin=1cm]
\item \hlred{How does the length of the man's arm in the replica compare with the length of the arm in the original?}

The length of arm would vary as length ratio $= 0.80$.

\item \hlred{How does the total surface area of the replica compare with the total surface area of the original?}

The surface area varies as length square, hence the ratio of the two areas would be $0.80^{2} \approx 0.64$. 
\end{enumerate}
\item \hlred{The earth has an equatorial radius of 3963 mi. (There are 5280 ft in one mile.) Imagine a string wrapped around the equator of a perfectly smooth earth. Suppose we now add 15ft to the length of the string and shape the longer string into a smooth circle with its center still at the center of the earth.}
\begin{marginfigure}
\centering
\includegraphics[width=0.5\textwidth]{figures/ch-01-02.jpg}
\caption{A string along the circumference of the earth.}
\label{fig-01-02}
\end{marginfigure}
\hlred{How far will the string now stand away from the surface of the earth? (Be sure to make the calculation in the simplest and most economical way; avoid doing irrelevant calculations and using irrelevant data. The sketch of an appropriate straight-line graph can be more helpful than a stream of words in explaining your line of reasoning.)}

\begin{marginfigure}
\centering
\includegraphics[width=0.95\textwidth]{figures/ch-01-pr02a.jpg}\\
\includegraphics[width=0.95\textwidth]{figures/ch-01-pr02b.jpg}
\caption{A string along the circumference of the earth. Plotting $\Delta C$ as a function of $\Delta C / 2 \pi$. Point $A$ in the graph represents $\Delta R$ which is equal to $\Delta C / 2 \pi$. The value of $a = \Delta C$}
\label{fig-01-02pra}
\end{marginfigure}

The circumference $C$ of a circle is given by $C = 2 \pi R$, where $R$ is the radius of the circle. Now our original string $(C_{1})$ has length equal to circumference of earth, or $C_{1} = 2 \pi R_{E}$, where $R_{E}$ is the equatorial radius of the earth. Now, the new string has an additional length of 15 feet, which also forms a circle with centre at the centre of earth. Let the new circumference be $C_{2}$ and radius of this new circle be $R_{N}$. Therefore we have $C_{2} = 2 \pi R_{N}$. We need to find $\Delta R = R_{N} - R_{E}$. For this
\begin{align*}
C_{2} - C_{1} & = 15 \, \text{feet}, \,\, \therefore \\
2 \pi R_{N} - 2 \pi R_{E} & = 15 \\
2 \pi (R_{N} - R_{E}) & = 15 \\
\Delta R & = \frac{15}{2 \pi} \approx 2.4 \, \text{feet}
\end{align*}

This additional length $\Delta R$ is \textbf{independent} of the original radius of earth. $\Delta R$ will be the same even if we consider the string around the sun with added 15 feet.

This is a highly \emph{counter-intuitive} result, as one expects that addition of such a small length (15 feet) to the circumference (\num{20924640} feet) will hardly change anything.

\item \hlred{Consider a bob on a string in uniform circular motion in a horizontal plane. Suppose that the tangential velocity $v_{t}$ of the bob is increased by a factor of 2.35 while the radius of the circle is increased by a factor of 1.76. The mass of the bob remains unchanged at \SI{145}{\gram}.}

\hlred{How does the final centripetal force $F_{\textrm{cf}}$ acting on the bob compare with the initial centripetal force $F_{\textrm{ci}}$?}

\hlred{In showing your line of reasoning, use the language of \emph{functional variation}: for example, in geometrical situations we argued that the area factor ``varies as the square of the length factor''). It is not appropriate to substitute the given numbers directly into a formula since the numbers are ratios and are not themselves velocities or radii. Avoid using any data that might be irrelevant.}

Centripetal force $F_{\textrm{c}}$ is given by  $F_{\textrm{c}} = m v_{t}^{2}r$, where $v_{t}$ is the tangential velocity at radius $r$ of the circular orbit. That is the force varies as square of the tangential velocity and linearly with the radius. Therefore the required ratio is
\begin{align*}%
\frac{F_{\textrm{cf}}}{F_{\textrm{ci}}} & = \frac{ m v_{tf}^{2}r_{f}}{ m v_{ti}^{2}r_{i}} \\ \marginnote{Subscripts $i$ and $f$ here denote initial and final quantities}
& = \frac{ v_{tf}^{2}r_{f}}{ v_{ti}^{2}r_{i}} \\
& = \left(\frac{v_{tf}}{ v_{ti}} \right)^{2} \times \left(\frac{r_{f}}{r_{i}} \right)\\
& = (2.35)^{2} \times (1.76) \\
& = 5.52 \times 1.76 \approx 9.71
\end{align*}

This result is independent of the mass of the bob.


\item \hlred{It is an empirical fact that the power output required of the engines of a boat or ship varies roughly as the cube of the velocity, that is, if you wish to double the velocity of the boat, you must increase the power output by a factor of eight.}

\hlred{Consider a twin-screw boat with a mass of 2.0 metric tons (one metric ton is equivalent to 1000 kg or 2200 lb). The boat is moving at an initial velocity $v_{i}$. The captain increases the power output of the engines by a factor of 2.6.}

\hlred{By what factor does he increase the kinetic energy of the boat, that is, how does the final kinetic energy $KE_{f}$ compare with the initial kinetic energy $KE_{i}$? (Explain your reasoning briefly; use the language of functional variation, not formulas; avoid using irrelevant data; evaluate the final numerical answer in decimal form, do not leave an unevaluated expression.)}

We know that power $P$ varies as cube of velocity $v$, that is $P \propto v^{3}$. From given data

\begin{align*}%
\frac{P_{f}}{P_{i}} & = 2.6 = \left(\frac{v_{f}}{v_{i}}\right)^{3} \\
\therefore \frac{v_{f}}{v_{i}} & = \sqrt[3]{2.6} \approx 1.37
\end{align*}
Now kinetic energy varies as square of the velocity, therefore
\begin{align*}%
\frac{KE_{f}}{KE_{I}} & =  \left(\frac{v_{f}}{v_{i}}\right)^{2} \\
 & = (1.37)^{2} \approx 1.87
\end{align*}



\end{enumerate}

\cleardoublepage